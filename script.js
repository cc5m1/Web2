    function numeroAleatorio() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
            const aleatorio = Math.floor(Math.random() * 10) + 1;
            if (aleatorio < 5) {
                resolve(aleatorio);
            } else {
                reject('O numero foi maior que 5');
            }
            }, 3000);
        });
        }
        
        
    async function atividade() {
        try {
            const result = await numeroAleatorio();
            console.log('Sucesso: ' + result);
            
        } catch (error) {
            console.error('Erro');
        }
        }
    
atividade();       
